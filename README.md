# InsightNets

### Instructions
The main analysis script is SwitchNetsBehavior.R.
The neural network simulations are in the folder Experiments/Simulations.

##### Requirements
R 4.3.1+, Python 3.8.5+

R packages: nloptr 2.0.3, reticulate 1.32.0, stats 4.3.1, lsr 0.5.2, plotly 4.10.2, plotrix 3.8-2, lme4 1.1-32, caret 6.0-94
Python packages: torch-2.1.0, numpy 1.24.4, pandas 1.1.3, nlopt 2.7.1, scipy 1.5.2

##### Scripts tested on
Operating systems: macOS Monterey 12.7.

### Instructions

Clone git repo:
git clone https://gitlab.com/aloewe/insightnets.git

Add Data folder from OSF into the repo:
https://osf.io/wh2r8/